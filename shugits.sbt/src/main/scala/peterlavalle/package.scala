package peterlavalle

import java.io.{File, FileWriter, StringWriter, Writer}
import java.util
import scala.annotation.targetName
import scala.io.Source
import scala.reflect.ClassTag


def stub[T]: T =
	val exception = scala.NotImplementedError()
	exception.setStackTrace(exception.getStackTrace.tail)
	(throw exception).asInstanceOf[T]

def stub[T](message: Any): T =
	val exception = scala.NotImplementedError(message.toString)
	exception.setStackTrace(exception.getStackTrace.tail)
	(throw exception).asInstanceOf[T]

val todo: Any => Unit = {
	val seen = new util.HashSet[String]()

	any =>
		val message = any.toString

		val exception = scala.NotImplementedError(message)
		exception.setStackTrace(exception.getStackTrace.tail.tail.tail)

		val trackHash =
			exception.getStackTrace.tail
				.take(4) // assume that we only need 5 frames to find it
				// ... so ... so this'll do fewer mesages than making ALL calls unique
				.foldLeft("")(_ + "," + _)
				.hashCode
				.toHexString

		exception.setStackTrace(exception.getStackTrace.take(3))

		val text = s"[ $trackHash ] $message"

		if (seen.add(text)) {
			val error = scala.NotImplementedError(text)
			error.setStackTrace(exception.getStackTrace)
			error.printStackTrace(System.err)
		}
}

extension [L, R](i: Iterable[(L, R)])
	def mapr[O](f: R => O): Iterable[(L, O)] =
		i.map {
			case (l, r) =>
				l -> f(r)
		}

extension [A <: AutoCloseable](autoCloseable: A)
	def useAndRelease[O](f: A => O): O = scala.util.Using.resource(autoCloseable)(f)

extension (s: String)
	def /(p: String): File = File(s) / p

extension (file: File)
	def PathTo(them: File): String =
		require(file.isDirectory)
		require(them.exists())

		def loop(file: List[String], them: List[String]): String =
			(file, them) match
				case ((fh :: ft), (th :: tt)) if fh == th =>
					loop(ft, tt)

		loop(
			file.AbsolutePath.split("/").toList,
			them.AbsolutePath.split("/").toList
		)

	def OverWrite(n: String => String = n => n)(f: String => Writer => Writer): Boolean =

		val src = Source.fromFile(file).useAndRelease(_.mkString)
		
		val w = f(src)

		if (null == w) false
		else {
			val s = new StringWriter()

			w(s).close()

			val out = s.toString


			if (n(src) == n(out))
				false
			else
				FileWriter(file)
					.append(out)
					.close()
				true
		}

	def AbsolutePath: String = AbsoluteFile.getAbsolutePath.replace('\\', '/')
	def AbsoluteFile: File =
		require(null != file)
		if (file == file.getAbsoluteFile)
			file
		else
			file.getAbsoluteFile

	def ParentFile: File = AbsoluteFile.getParentFile
	def EnsureParent: File =
		require(ParentFile.isDirectory || ParentFile.mkdirs())
		AbsoluteFile

	@targetName("sub")
	def /(name: String): File = new File(file, name).AbsoluteFile

	def ls: Seq[String] =

		val file = AbsoluteFile
		if (file != file.AbsoluteFile)
			file.AbsoluteFile.ls
		else
			require(file.exists())
			require(file.isDirectory, s"can't ls ${file.AbsolutePath}")
			val list = file.list()
			if (null == list)
				Seq()
			else
				list.map {
					child =>
						if ((file / child).isDirectory)
							child + "/"
						else
							child
				}

	def lsa: LazyList[String] =
		val file = AbsoluteFile
		file.ls.to(LazyList).flatMap {
			case folder if folder.endsWith("/") =>
				(file / folder).lsa.map(folder + _)
			case sub =>
				LazyList(sub)
		}

/**
 * perform a check (of the macro'ed text) but continue even if it fails
 */
inline def suspect(inline condition: Boolean): Unit =
	if (!condition) {
		val source: String = macros.suspect(condition)
		val exception: AssertionError = java.lang.AssertionError(source)
		exception.setStackTrace(exception.getStackTrace)

		val trackHash: String =
			exception.getStackTrace.tail
				.take(4) // assume that we only need 5 frames to find it
				// ... so ... so this will do fewer messages than making ALL calls unique
				.foldLeft("")((_: String) + "," + (_: StackTraceElement))
				.hashCode
				.toHexString

		exception.setStackTrace(exception.getStackTrace.take(3))

		val message: String = s"[ $trackHash ] `$source` failed"

		if (true) { //seen.add(text)) {
			val error: AssertionError = java.lang.AssertionError(message)
			error.setStackTrace(exception.getStackTrace)
			error.printStackTrace(System.err)
		}
	}

private object macros {

	import scala.quoted.*

	private[peterlavalle] inline def suspect(inline condition: Boolean): String = ${ suspect0('condition) }

	private def suspect0(condition: Expr[Boolean])(using Quotes): Expr[String] = Expr(condition.show)
}
