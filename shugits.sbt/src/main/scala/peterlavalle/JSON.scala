package peterlavalle

import org.json.{JSONArray, JSONObject}
import peterlavalle.JSON.tParse

/**
 * implements a sort-of DSL to parse/read/load JSON.
 *
 * @tparam T the value loaded via this object
 */
trait JSON[T] extends JSON.tParse[T] with (JSONObject => T):
	override def from(jsonObject: JSONObject, key: String): Either[T, Exception] =
		parse(jsonObject.getJSONObject(key))

	def parse(json: JSONObject): Either[T, Exception]


	type Chain = Either[List[T], Exception]


	def parse(json: JSONArray): Chain =
		(0 until json.length())
			.map(json.getJSONObject)
			.foldLeft(Left(List[T]()): Chain) {
				case (left: Chain, json) =>

					left.left.flatMap {
						(lead: List[T]) =>
							parse(json).left.map {
								(last: T) =>
									lead :+ (last)
							}
					}
			}

	def apply(o: JSONObject): T =
		parse(o).left.getOrElse {
			sys.error("failed to parse " + o)
		}

object JSON:


	// basic building block of the concept here
	trait tBlock[T] {
		def flatMap[O](f: T => JSON[O]): JSON[O]

		def map[O](f: T => O): JSON[O]
	}

	private case class KeyMissing(key: String) extends Exception

	trait tParse[T]:
		def from(jsonObject: JSONObject, key: String): Either[T, Exception]


		def from(jsonArray: JSONArray, idx: Int): Either[T, Exception] = {
			// cheat! move the value to a random key, then, extract that value
			val key: String = "key" + Exception().getStackTrace
				.foldLeft("")((_: String) + (_: StackTraceElement))
				.hashCode.toHexString
			from(new JSONObject().put(key, jsonArray.get(idx)), key)
		}

	given tParse[Int] =
		(jsonObject: JSONObject, key: String) =>
			Left(jsonObject.getInt(key))

	given tParse[String] =
		(jsonObject: JSONObject, key: String) =>
			Left(jsonObject.getString(key))

	extension (key: String)
		def apply[T: tParse]: tBlock[T] = field(key).apply[T]
		def array[T: tParse]: tBlock[List[T]] =

			given tParse[List[T]] =
				(jsonObject: JSONObject, key: String) =>
					val array: JSONArray = jsonObject.getJSONArray(key)
					(0 until array.length())
						.map(summon[tParse[T]].from(array, _))
						.foldLeft(Left(List()): Either[List[T], Exception]) {
							case (list, next) =>
								for {
									list <- list.left
									next <- next.left
								} yield {
									list ++ List(next)
								}
						}

			apply[List[T]]

	//import scala.quoted.*
	class Q[T] private[JSON](parser: tParse[T]) {
		//inline def flatMap[O](inline f: T => JSON[O]): JSON[O] =
		//	${ JSON.bind[T, O]('this, 'f) }
		//inline def map[O](inline f: T => O): JSON[O] =
		//	${ JSON.pure[T, O]('this, 'f) }
		def pure[O](key: String, f: T => O): JSON[O] =
			(json: JSONObject) =>
				if (!json.has(key))
					Right(KeyMissing(key))
				else
					parser.from(json, key)
						.left.map(f)

		def bind[O](key: String, f: T => JSON[O]): JSON[O] =
			(json: JSONObject) =>
				if (!json.has(key))
					Right(KeyMissing(key)): Either[O, Exception]
				else
					parser.from(json, key)
						.left.map(f)
						.left.flatMap(_.parse(json))
	}
	//import scala.quoted.*
	//def bind[T, O](t: Expr[Q[T]], f: Expr[T => JSON[O]])(using Quotes): Expr[JSON[O]] =
	//	val key = f.show.dropWhile('(' == _).takeWhile(_ != ':')
	//	t.valueOrAbort.bind[O](key, f.valueOrAbort)
	//def pure[T, O](t: Expr[Q[T]], f: Expr[T => O])(using Quotes): Expr[JSON[O]] =
	//	val key = f.show.dropWhile('(' == _).takeWhile(_ != ':')
	//	t.valueOrAbort.pure[O](key, f.valueOrAbort)
	//def get[T: tParse]: Q[T] = Q(summon[tParse[T]])

	class tField private[JSON](key: String) {
		def apply[T: tParse]: tBlock[T] =
			new tBlock[T]:
				override def flatMap[O](f: T => JSON[O]): JSON[O] =
					Q(summon[tParse[T]]).bind(key, f)

				override def map[O](f: T => O): JSON[O] =
					Q(summon[tParse[T]]).pure(key, f)
	}


	/**
	 * reads a key'ed field - intended to be used in a for{}yield{} expression
	 *
	 * if I had more time/knowledge I'd be able to make the key parameter implicit via a macro that scans the `key <-` thing
	 *
	 * @param key
	 * @return
	 */
	def field(key: String): tField = tField(key)
	
		