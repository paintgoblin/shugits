package peterlavalle.shugits

import com.jcraft.jsch.{JSch, KeyPair}
import com.typesafe.scalalogging.LazyLogging
import peterlavalle.*

object TempForgeIdentity extends LazyLogging:


	type TemporaryPair = (String, String)

	extension (tp: TemporaryPair)
		def pri: String = tp._1

	def tempIdentity[Q](fg: ForgeJoCalls, owner: String, tag: String)(f: TemporaryPair => Q): Q =
		val pair@(privateKey, publicKey) = generateKeyPair(tag)

		val (_ :: pub :: _) = publicKey.split(" ").map(_.trim).toList

		// install the key on $owner
		logger.info(s"adding temp key >$tag< to `$owner` ...")
		fg.user(owner).keys += tag -> pub
		logger.info(s"... it added. executing the pair")
		try
			try
				f(pair)
			finally
				logger.info(s"pair successful")
		finally
			// remove the key from $owner
			fg.user(owner).keys -= pub
			logger.info(s"removed $owner's $tag")


	private def generateKeyPair(tag: String): TemporaryPair =
		val jSch = new JSch()
		val keyPair = KeyPair.genKeyPair(jSch, KeyPair.RSA, 1024 * 4)

		val privateKeyStream = new java.io.ByteArrayOutputStream()
		val publicKeyStream = new java.io.ByteArrayOutputStream()

		keyPair.writePrivateKey(privateKeyStream)
		keyPair.writePublicKey(publicKeyStream, tag)

		val privateKey = privateKeyStream.toString("UTF-8")
		val publicKey = publicKeyStream.toString("UTF-8")

		(privateKey, publicKey)
