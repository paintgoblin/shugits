package peterlavalle.shugits

import org.apache.sshd.common.session.Session
import org.apache.sshd.common.util.net.SshdSocketAddress
import org.apache.sshd.server.forward.{ ForwardingFilter, TcpForwardingFilter }

object NoPipesForwardingFilter extends ForwardingFilter {
  override def canForwardAgent(session: Session, requestType: String): Boolean =
    false

  override def canListen(address: SshdSocketAddress, session: Session): Boolean =
    false

  override def canConnect(tcpFilter: TcpForwardingFilter.Type, address: SshdSocketAddress, session: Session): Boolean =
    false

  override def canForwardX11(session: Session, requestType: String): Boolean =
    false
}
