package peterlavalle.shugits

import com.jcraft.jsch.{JSch, Session}
import org.eclipse.jgit.api.{Git, PushCommand}
import org.eclipse.jgit.transport.ssh.jsch.{JschConfigSessionFactory, OpenSshConfig}
import org.eclipse.jgit.transport.{SshTransport, Transport}

import java.io.File

object IdentifiedPush:
	def apply(repo: File)(privateKeyText: String): PushCommand =
		val pushCommand: PushCommand = Git.open(repo).push()
		pushCommand.setTransportConfigCallback {
			case (sshTransport: SshTransport) =>
				sshTransport.setSshSessionFactory(
					new JschConfigSessionFactory() {
						override def configure(host: OpenSshConfig.Host, session: Session): Unit = {}

						override protected def createDefaultJSch(fs: org.eclipse.jgit.util.FS): JSch = {
							val jSch = super.createDefaultJSch(fs)
							jSch.addIdentity("id_rsa", privateKeyText.getBytes("UTF-8"), null, null)
							jSch
						}
					}
				)

		}
		pushCommand

