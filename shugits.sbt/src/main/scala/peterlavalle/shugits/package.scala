package peterlavalle

import org.json.JSONArray

import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.Date

package object shugits:
	extension (s: String)
		def splitLines: List[String] = s.split("[\r \t]*\n").toList
		def foldLines(left: String)(join: (String, String) => String): String =
			s.splitLines.foldLeft(left)(join)

	extension (date: Date)
		def iso8601: String =
			DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(date.toInstant.atZone(ZoneId.systemDefault()))

	extension (json: JSONArray)
		def toSeq[T](f: (JSONArray, Int) => T): Seq[T] =
			(0 until json.length()).map(f(json, _: Int))

	extension [T](i: Iterable[T])
		def toJSON(f: (JSONArray, T) => JSONArray): JSONArray =
			i.foldLeft(JSONArray())(f)
