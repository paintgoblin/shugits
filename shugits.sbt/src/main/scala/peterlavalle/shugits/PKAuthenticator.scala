package peterlavalle.shugits

import com.typesafe.scalalogging.LazyLogging
import org.apache.sshd.common.AttributeRepository
import org.apache.sshd.server.auth.pubkey.PublickeyAuthenticator
import org.apache.sshd.server.session.ServerSession

import java.security.PublicKey
import scala.util.chaining.*

/**
 * autheticate the name matches a pattern and store the public key for later.
 *
 * ... the forgeo API doesn't let me/us search-by-key so we just let everyone in and hope we can survive until they tell us what they want to do#
 *
 * @param names
 */
class PKAuthenticator(names: String) extends PublickeyAuthenticator with LazyLogging {

	override def authenticate(username: String, key: PublicKey, session: ServerSession): Boolean =
		if (!username.matches(names))
			logger.info(s"rejecting ssh user $username")
			false
		else {
			session.setAttribute(PKeyAttribute, key)
			logger.info(s"accepting ssh user $username")
			true
		}


	case object PKeyAttribute extends AttributeRepository.AttributeKey[PublicKey]
}