package peterlavalle.shugits

import com.typesafe.scalalogging.LazyLogging
import org.json.{JSONArray, JSONObject}
import peterlavalle.*

import java.io.*
import scala.annotation.targetName
import scala.sys.process.*

class HgActions(root: File) extends LazyLogging:
	assert(root == root.AbsoluteFile)
	assert((root / ".hg").isDirectory)
	
	type Bookmark = String
	type Branch = String
	type Node = String

	def log(r: String): List[JSONObject] =
		val json: JSONArray =
			JSONArray {
				Process(
					Seq("hg", "log", "-Tjson", s"-r$r"),
					root
				).!!
			}
		(0 until json.length())
			.toList
			.map(json.getJSONObject)

	def heads(): List[(Branch, Node)] =
		import JSON.{*, given}
		log("head() and not closed()")
			.map {
				for {
					node <- field("node")[String]
					branch <- field("branch")[String]
				} yield {
					branch -> node
				}
			}

	def parent: JSONObject =
		log(".") match
			case List(head) => head

	object bookmark:
		// TODO; use some dumb prefix like "shugits-internral-????" and keep/ignore the others

		@targetName("remove")
		def -=(name: Bookmark): Unit =
			assert(0 == {
				// hg bookmarks --delete
				Process(
					Seq("hg", "bookmark", "--delete", name),
					root
				).!
			})

		def update(node: Node, name: Bookmark): Unit =

			val r = {
				Process(
					Seq("hg", "bookmark", "-f", "-r", name, node),
					root
				).!
			}

			assert(0 == r, s"got r=$r when trying to bookmark `$name` on $node")


		def apply(): List[(Bookmark, Node)] =
			log("bookmark()")
				.flatMap {
					(json: JSONObject) =>

						// TODO; replace/doop this with more-good JSON when that can handle .array[T] along with .field[T]

						val array: JSONArray = json.getJSONArray("bookmarks")

						(0 until array.length())
							.map(array.getString)
							.map((_: Bookmark) -> json.getString("node"))

				}
