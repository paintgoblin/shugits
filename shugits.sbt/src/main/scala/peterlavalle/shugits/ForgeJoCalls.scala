package peterlavalle.shugits

import org.json.{JSONArray, JSONObject, JSONTokener}
import peterlavalle.*

import java.io.{FileNotFoundException, InputStream, OutputStream}
import java.net.{HttpURLConnection, URL}
import java.nio.charset.StandardCharsets
import scala.annotation.targetName
import scala.util.Using;

class ForgeJoCalls(api: String, token: String) {
	class user(name: String):
		object keys:
			def apply(): Seq[String] =
				swag.sequence(s"users/$name/keys")
					.map((_: JSONObject).getString("key"))

			@targetName("assign")
			def +=(titleRsa: (String, String)): Unit =
				val (title: String, rsa: String) = titleRsa
				swag.postJson(s"admin/users/$name/keys")(
					new JSONObject()
						.put("title", title)
						.put("key", rsa)
						.put("read_only", false)
				) match
					case code =>
						require(201 == code, s"adding public key got $code")

			@targetName("remove")
			def -=(rsa: String): Unit =
				swag
					.sequence(s"users/$name/keys")
					.filter {
						(json: JSONObject) =>
							@unchecked
							val (_ :: pub :: _) = json.getString("key").split(" ").map(_.trim).toList
							rsa.trim == pub.trim
					}
					.map((_: JSONObject).get("id")) match
					case List(id) =>

						val connection =
							new URL(s"$api/api/v1/user/keys/$id")
								.openConnection()
								.asInstanceOf[HttpURLConnection]

						connection.setRequestMethod("DELETE")
						connection.setRequestProperty("Authorization", s"token $token")
						connection.setDoOutput(true)

						try {
							if (connection.getResponseCode == 204) {
								()
							} else {
								stub
								//val errorStream = connection.getErrorStream
								//val errorResponse = new String(errorStream.readAllBytes(), StandardCharsets.UTF_8)
								//println(s"Error deleting SSH key: $errorResponse")
							}
						} finally {
							connection.disconnect()
						}


					case list =>
						sys.error(
							"found " + list.length + " keys that match ... that seems wrong"
						)

		def exists: Boolean =
			try {
				swag.single(s"users/$name").has("id")
			} catch {
				case fileNotFoundException: FileNotFoundException =>
					false
			}

	def projectExists(owner: String, project: String): Boolean =
		try {
			swag.single(s"repos/$owner/$project")
				.has("id")
		} catch
			case _: FileNotFoundException =>
				false

	def projectCollaborators(owner: String, project: String): Seq[String] =
		swag.sequence(s"repos/$owner/$project/collaborators")
			.map((_: JSONObject).getString("username"))

	def projectCollaborate(owner: String, project: String, newguy: String): Unit =
		swag.postJson(s"repos/$owner/$project/collaborators/$newguy", "PUT")(
			JSONObject()
				.put("permission", "write")
				.put("role_name", "write")
		) match
			case 204 => ()
			case code =>
				stub(s"code = $code")

	private object swag:
		def sequence(url: String): List[JSONObject] =
			new URL(s"$api/api/v1/$url?token=$token")
				.openStream()
				.useAndRelease {
					(stream: InputStream) =>
						val array: JSONArray = JSONArray(JSONTokener(stream))

						(0 until array.length())
							.map(array.getJSONObject)
							.toList
				}

		def single(url: String): JSONObject =
			new URL(s"$api/api/v1/$url?token=$token")
				.openStream()
				.useAndRelease {
					(stream: InputStream) =>
						JSONObject(JSONTokener(stream))
				}

		def postJson(path: String)(json: JSONObject): Int =
			postJson(path, "POST")(json)

		def postJson(path: String, method: String)(json: JSONObject): Int = {

			val url: URL = new URL(s"$api/api/v1/$path?token=$token")
			// Create a URL object and open a connection
			val connection: HttpURLConnection =
				url
					.openConnection()
					.asInstanceOf[HttpURLConnection]

			try {
				// Set the request method
				connection.setRequestMethod(method)

				// Set request headers
				connection.setRequestProperty("accept", "application/json")
				connection.setRequestProperty("Content-Type", "application/json")
				connection.setDoOutput(true)

				// Writing JSON payload to output stream
				Using(connection.getOutputStream) {
					(_: OutputStream).write(json.toString.getBytes(StandardCharsets.UTF_8))
				}.getOrElse(throw new RuntimeException("Error writing JSON payload to output stream"))

				// Reading response
				connection.getResponseCode

			} finally {
				// Disconnect the connection
				connection.disconnect()
			}
		}
}
