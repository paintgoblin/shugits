package peterlavalle.shugits

import peterlavalle.*

import java.io.{File, FileWriter}

object ShellScript:
  def apply(file: File)(script: OS => String): File =

    // determin os
    val os: OS = System.getProperty("os.name").toLowerCase.take(3) match
      case "win" => OS.Win
      case "lin" => OS.Lin

    // findget the file
    val out: File =
      os match
        case OS.Win => File(file.AbsolutePath + ".cmd").AbsoluteFile
        case OS.Lin => File(file.AbsolutePath + ".sh").AbsoluteFile

    // get the script souir
    val source = script(os)
      // trim the lines
      .replace("[\r \t]*\n", "\n").trim
    
    // just alwaus write it
    FileWriter(out.EnsureParent)
      .append(source)
      .close()

    assert(
      out.canExecute || {
        out.setExecutable(true)
      }
    )
    out

  enum OS:
    // there are more than these two, but, i don't have the equipment to test them
    case Win
    case Lin
