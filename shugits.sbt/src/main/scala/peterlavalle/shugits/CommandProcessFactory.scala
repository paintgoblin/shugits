

package peterlavalle.shugits

import org.apache.sshd.server.channel.ChannelSession
import org.apache.sshd.server.command.{Command, CommandFactory}
import org.apache.sshd.server.shell.{InvertedShellWrapper, ProcessShell}
import org.apache.sshd.server.{Environment, ExitCallback}

import java.io.{InputStream, OutputStream}
import scala.beans.BeanProperty

object CommandProcessFactory {

	case class Context
	(
		channel: ChannelSession,
		command: String,
		environment: Environment,
		outputStream: OutputStream,
		errorStream: OutputStream,
		inputStream: InputStream
	)

	def apply(f: Context => Seq[String]): CommandFactory =
		(channel: ChannelSession, command: String) =>

		// intercepting stderr and choosing where to log it to would be ... good ... and beyond my interest

			new Command {

				@BeanProperty
				var outputStream: OutputStream = _

				@BeanProperty
				var errorStream: OutputStream = _

				@BeanProperty
				var inputStream: InputStream = _

				@BeanProperty
				var exitCallback: ExitCallback = _

				var invertedShellWrapper: InvertedShellWrapper = _

				override def start(channel: ChannelSession, env: Environment): Unit =

					val cmd = f(Context(channel, command, env, outputStream, errorStream, inputStream))

					invertedShellWrapper = new InvertedShellWrapper(new ProcessShell(cmd *))

					invertedShellWrapper.setOutputStream(outputStream)
					invertedShellWrapper.setErrorStream(errorStream)
					invertedShellWrapper.setInputStream(inputStream)
					invertedShellWrapper.setExitCallback(exitCallback)

					invertedShellWrapper.start(channel, env)

				override def destroy(channel: ChannelSession): Unit =
					invertedShellWrapper.destroy(channel)
			}
}
