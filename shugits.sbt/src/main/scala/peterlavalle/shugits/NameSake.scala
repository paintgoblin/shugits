package peterlavalle.shugits

import com.typesafe.scalalogging.LazyLogging
import peterlavalle.*

import java.io.{File, FileWriter}
import scala.io.{BufferedSource, Source}
import scala.sys.process.*

/**
 * creates and configures hg and git "namesake" repos
 *
 * @param root
 */
class NameSake(root: File)(push: ShellScript.OS => String) extends LazyLogging {

	def hg(owner: String, project: String): File =
		val file: File = root / owner / project / "hg-namesake"

		val pushScriptFile: File =
			ShellScript(root / "run_the_hook")(push)
		

		// create and setup the repo
		if (!file.isDirectory) {

			// mkdir
			assert(
				file.mkdirs()
			)

			// hg init
			assert {
				0 ==
					Process(
						command = Seq("hg", "init", "./"),
						cwd = file
					).!
			}
		}


		// write the hgrc
		// just always overwrite it to deal with the/a/my issue with backups
		val hgrc: File = file / ".hg" / "hgrc"
		if (hgrc.exists())
			hgrc.delete()
		new FileWriter(hgrc)
			.append(
				s"""
					 |[extensions]
					 |hgext.bookmarks =
					 |hggit =
					 |
					 |[hooks]
					 |changegroup.push-forgejo = "${pushScriptFile.AbsolutePath}" $owner $project
					 |
					 |""".stripMargin
			)
			.close()
		assert(hgrc.exists())

		assert(
			(file / ".hg").isDirectory,
			s"no `.hg/` in `${file.AbsolutePath}`"
		)
		file

	def git(owner: String, project: String): File =
		val file: File = root / owner / project / "git-namesake"

		// create/find the .git-namesake copy
		if (!file.isDirectory) {
			assert(file.mkdirs())
			assert {
				0 ==
					Process(
						command = Seq("git", "init", "--bare", "./"),
						cwd = file

					).!
			}
		}
		file
}
