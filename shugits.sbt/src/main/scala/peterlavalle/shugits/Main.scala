package peterlavalle.shugits

import com.typesafe.scalalogging.LazyLogging
import org.apache.sshd.server.SshServer
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider
import org.eclipse.jgit.transport.RefSpec
import peterlavalle.*
import scopt.OParserBuilder

import java.io.{File, StringWriter}
import java.lang
import java.lang.System
import java.security.PublicKey
import java.util.Date
import scala.io.{BufferedSource, Source}
import scala.sys.process.*
import scala.util.matching.Regex

object Main:
	def main(args: Array[String]): Unit =
		System.setProperty("slf4j.internal.verbosity", "WARN") // see: https://github.com/qos-ch/slf4j/issues/422
		import scopt.OParser
		OParser
			.parse({
				val builder: OParserBuilder[Main] = OParser.builder[Main]
				import builder.*
				OParser.sequence(

					programName("shugits"),
					head("scala hg git sync", "2024-05-23"),

					opt[File]("namesake")
						.text("the folder to user for the namesake-repos")
						.action((namesake: File, app: Main) => app.copy(nameSake = namesake)),

					opt[String]("api")
						.text("the path/url to the swagger API for forgejo")
						.action((api: String, app: Main) => app.copy(api = api)),

					opt[File]("token")
						.text("file with API token for forgejo")
						.action((token: File, app: Main) => app.copy(tokenFile = token)),

					opt[String]("ssh")
						.text("the (prefix) of the forgejo's git ssh connection")
						.action((ssh: String, app: Main) => app.copy(ssh = ssh)),

					cmd("push")
						.text("push something to forgejo")
						.action((_: Unit, main: Main) => main.copy(command = "push"))
						.children(
							arg[String]("owner")
								.required()
								.action((owner: String, main: Main) => main.copy(owner = owner)),
							arg[String]("project")
								.required()
								.action((project: String, main: Main) => main.copy(project = project)),
						),

					cmd("sshd")
						.text("run/launch the SSHD")
						.action((_: Unit, main: Main) => main.copy(command = "sshd"))
						.children(

							opt[File]("ser")
								.text("hostKey to identify shugits")
								.action((ser: File, app: Main) => app.copy(ser = ser)),

							opt[Int]("port")
								.text("port to listen on (funniest when it matches forgejo's port)")
								.action((port: Int, app: Main) => app.copy(port = port)),

						)
				)
			},
				args,
				Main()
			).get.run()

case class Main
(
	tokenFile: File = "target" / "shugits.token",
	ser: File = "target" / "shugits.ser",
	port: Int = 5190,
	api: String = "http://localhost:3000",
	nameSake: File = "target" / "namesake-repos",
	ssh: String = "ssh://git@localhost:2200",

	owner: String = null,
	project: String = null,

	command: String = "sshd"
) extends LazyLogging {

	private def run(): Unit = {
		command match
			case "sshd" =>
				logger.info("sshd starting ...")
				checks()
				sshd()
			case "push" =>
				logger.info("push starting ...")
				push()
			case unknown =>
				logger.info(s"unknown command `$unknown`")
	}

	/**
	 * checks of the configuration
	 */
	private def checks(): Unit = {

		logger.info(
			"ser := " + ser,
			"port := " + port,
			"api := " + api,
			"nameSake := " + nameSake,
			"ssh := " + ssh,
		)

		// check for hggit
		if (true) {
			val config: String = {

				try {
					val cmd: String = "hg showconfig extensions"

					val out: StringWriter = new StringWriter()

					val r: Int =
						Process(
							command = cmd,
						) ! ProcessLogger(
							(o: String) =>
								out.append(s";$o\n"),
							(e: String) =>
								out.append(s"!$e\n"),
						)

					logger.info(
						out.toString.foldLines(cmd)((_: String) + "\n\t" + (_: String))
					)

					assert(
						r == 0,
						out.toString.foldLines("r = " + r)(
							(_: String) + "\n\t; " + (_: String)
						)
					)
					out.toString
				} catch
					case e: Throwable =>
						throw Exception("failed to query hg extensions?", e)
			}
			assert(
				config.contains("extensions.hggit="),
				config.foldLines(
					"hggit not installed?"
				)((_: String) + "\n\t>" + (_: String) + "<")
			)
		}

		// setup the host key
		if (true) {
			val rHost: Regex = """ssh://(\w+)@(\w+):(\d+)""".r

			@unchecked
			val rHost(user, host, port) = ssh

			Seq(
				"ssh", "-o", "StrictHostKeyChecking=accept-new", s"$user@$host", s"-p$port"
			).!
		}

		// validate that there's a token file
		while (!tokenFile.isFile) {
			logger.info("need token file " + tokenFile.AbsolutePath)
			Thread.sleep(3140)
		}
		assert(
			tokenFile.isFile,
			"need token file " + tokenFile.AbsolutePath
		)

		// be sure we can reach forgejo
		assert(
			"<title>Forgejo API</title>" == {
				import java.net.URL
				Source.fromInputStream(
					new URL(s"$api/api/swagger")
						.openStream()
				).useAndRelease(
					(_: BufferedSource).mkString.trim
						.split("([\r \t]*\n)+")
						.apply(3)
						.trim
				)
			},
			s"can't reach forgejo swagger/server/http `$api`"
		)
	}

	/**
	 * this runs our facsimile of an SSHd server to host
	 */
	private def sshd(): Unit = {

		import submodules.*

		// star creating the sshd server
		val sshd = SshServer.setUpDefaultServer()

		// run on port
		sshd.setPort(port)

		// use a simple "what host are you?" kep-pair thing
		sshd.setKeyPairProvider(
			SimpleGeneratorHostKeyProvider(ser.toPath)
		)

		// authenticate on name and store public key
		sshd.setPublickeyAuthenticator(authenticator)

		// what is GSS? idk - don't use it
		sshd.setGSSAuthenticator(null)

		// don't allow passwords
		sshd.setPasswordAuthenticator(null)
		sshd.setKeyboardInteractiveAuthenticator(null)

		// don't allow shells
		sshd.setShellFactory(null)

		// block pipes
		sshd.setForwarderFactory(null)

		// the main functionality oif the "server" is done by this command factory
		sshd.setCommandFactory {

			val cc = """([\w\-_\.]+)"""

			val rHgServe: Regex = s"hg -R $cc/$cc(\\.git)? serve --stdio".r

			val rGitPack: Regex = s"git-receive-pack '/$cc/$cc(\\.git)?'".r

			CommandProcessFactory {
				(context: CommandProcessFactory.Context) =>
					given CommandProcessFactory.Context = context

					context.command match
						case rHgServe(owner, project, _) =>

							// ensure that this user can access to repo
							// TODO; allow "deploy keys" (maybe not - deploy from git you slobs)
							// TODO; allow other users to access eachother's repos (maybe not - i hate the practice IRL)
							// TODO; allow organisations (sorry)
							val not_suc =
								val publicKey: PublicKey = context.channel.getSession.getAttribute(authenticator.PKeyAttribute)
								logger.info(publicKey.toString)
								!sessionUserCheck(publicKey, owner)

							if (not_suc)
								throw RuntimeException(s"you are forbidden from $owner / $project")


							// check to be sure that the forgejo copy exists
							if (!forgejoCalls.projectExists(owner, project))
								throw RuntimeException(s"tried to connect to $owner / $project which doesn't exist")

							// find/load/create/whatever the hg repo
							val theRepo = nameSakes.hg(owner, project)

							// the command!
							List("hg", "-R", theRepo.AbsolutePath, "serve", "--stdio")
			}
		}

		// start it and just wait for it to finish
		sshd.start()
		logger.info("sHugITs SSH server started")
		while (!sshd.isClosed)
			Thread.sleep(10)
	}

	/**
	 * this is invoked internally as a hook when data is pushed on/from the sshd server
	 */
	private def push(): Unit = {

		// doing something to signal that we're passing through the sshd() thing would be good

		logger.info("owner/project = " + owner + " / " + project)

		val hgNameSake = submodules.nameSakes.hg(owner, project)

		// apply bookmarks
		RepoCalls.markLabel(HgActions(hgNameSake), true)

		// create/find the .git-namesake copy
		val gitNamesake: File = submodules.nameSakes.git(owner, project)

		// push bookmarks to .git-namesake
		RepoCalls.hgPushGit(
			hgNameSake,
			gitNamesake
		)

		// remove the bookmarks now to prevent them from fluttering down to the user
		RepoCalls.removeAllBookmarks(hgNameSake)

		val tag = "temporary shugits key " + Date().iso8601

		import TempForgeIdentity.*

		tempIdentity(submodules.forgejoCalls, owner, tag) {
			(pair: TemporaryPair) =>

				val specs: List[RefSpec] =
					RepoCalls.refSpecs(gitNamesake) {
						(branch: String) =>
							assert(branch.startsWith("hg."))
							branch.substring(3)
					}

				IdentifiedPush(gitNamesake)(pair.pri)
					.setForce(true)
					.setRemote(s"$ssh/$owner/$project.git")
					.setRefSpecs(specs *)
					.call()

		}
	}

	// magic submodules
	private object submodules {

		lazy val tokenString: String = {
			val tokenString: String = Source.fromFile(tokenFile).useAndRelease(_.mkString.trim)
			assert(tokenString.length == 40, "the .token file is the wrong size")
			tokenString
		}
		lazy val forgejoCalls = ForgeJoCalls(api, tokenString)
		lazy val sessionUserCheck = SessionUserCheck(forgejoCalls)
		lazy val nameSakes = {
			val cp =
				System.getProperty("java.class.path").trim.split(";")
					.toList
					.map(_.trim).filter(_.nonEmpty)
					.map(File(_).AbsolutePath)
					.reduce(_ + File.pathSeparator + _)

			val opts =
				// this will pass these parameters into the hook
				List(
					"namesake" -> nameSake.AbsolutePath,
					"api" -> api,
					"token" -> tokenFile.AbsolutePath,
					"ssh" -> ssh,
				).map("--" + _ + " " + _).foldLeft("")(_ + " " + _)

			NameSake(nameSake) {
				case ShellScript.OS.Win =>
					s"""
						 |@ECHO OFF
						 |java -cp "$cp" ${classOf[Main].getName} push %1 %2 $opts
				""".stripMargin.trim

				case ShellScript.OS.Lin =>
					s"""
						 |#!/usr/bin/env bash
						 |
						 |java -cp "$cp" ${classOf[Main].getName} push $$1 $$2 $opts
				""".stripMargin.trim

				case what =>
					throw RuntimeException(s"who do on os $what?")
			}
		}
		val authenticator: PKAuthenticator = PKAuthenticator("(hg|git)")
	}
}
