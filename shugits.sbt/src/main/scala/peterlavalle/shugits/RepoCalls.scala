package peterlavalle.shugits

import com.typesafe.scalalogging.LazyLogging
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.Ref
import org.eclipse.jgit.transport.RefSpec
import peterlavalle.*

import java.io.File
import scala.sys.process.*
import scala.util.matching.Regex

object RepoCalls extends LazyLogging {
	def hgPushGit(hgNameSake: File, gitNamesake: File): Unit = {
		val r: Int = {
			Process(
				command = Seq(
					"hg", "push", gitNamesake.AbsolutePath
				),
				cwd = hgNameSake.AbsoluteFile
			).!
		}
		assert(
			0 == r || 1 == r,
			s"hg-namesake push to git-namesake got a $r"
		)
	}

	def refSpecs(gitNamesake: File)(reName: String => String): List[RefSpec] =
		Git.open(gitNamesake)
			.branchList().call().toArray
			.toList
			.map {
				case (ref: Ref) =>

					val rRefName: Regex = """refs/heads/hg\.(.+)""".r
					val rBadNode: Regex = """refs/heads/hg-(.+)""".r

					ref.getName match
						case rRefName(name) =>
							s"hg.$name"
						case rBadNode(node) =>
							logger.info(
								s"WARNING - multiple heads with the same name - git can't see $node"
							)
							null
			}
			.filter(_ != null)
			.map((head: String) => head -> reName(head))
			.filterNot(null == _._2)
			.map {
				(head, upto) =>
					new RefSpec(head + ":" + upto)
			}


	def removeAllBookmarks(hgNameSake: File | HgActions): Unit =
		val hg: HgActions =
			hgNameSake match
				case file: File =>
					HgActions(file)
				case hg: HgActions => hg

		hg.bookmark().foreach {
			case (bookmark, _) =>
				hg.bookmark -= bookmark
		}


	def markLabel
	(
		hg: HgActions,
		live: Boolean,
	): Unit = {

		assert(live, "live/not-live is old functionality tht i'm ksipping")

		//
		val heads: Seq[(hg.Branch, hg.Node)] = hg.heads()

		// compute head names
		val bookmarks: Map[String, hg.Node] =
			heads
				.groupBy((_: (hg.Branch, hg.Node))._1)
				.flatMap {

					case (branch: hg.Branch, Seq((_, name: hg.Node))) =>
						Seq(("hg." + branch) -> name)

					case (branch: hg.Branch, many: Seq[(hg.Branch, hg.Node)]) =>
						many
							.map((_: (hg.Branch, hg.Node))._2)
							.map {
								(node: hg.Node) =>
									s"hg-$node" -> node
							}

				}

		// remove OLD bookmark
		removeAllBookmarks(hg)

		// finally - apply the new bookmarks
		bookmarks.foreach {
			case (bookmark, node) =>
				if (!live)
					logger.info("would add " + (bookmark, node))
				else
					hg.bookmark(bookmark) = node
		}
	}
}
