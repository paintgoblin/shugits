package peterlavalle.shugits

import peterlavalle.*

import java.security.PublicKey
import java.text.MessageFormat

class SessionUserCheck(swagForge: ForgeJoCalls) {

	def apply(publicKey: PublicKey, userName: String): Boolean =

		//		// check they're not banished
		//		val userRecord = swagForge.getUser(userName)
		//		if (!userRecord.isActive || userRecord.isProhibitLogin)
		//			stub(
		//				"sir you are forbidden"
		//			)

		// find their keys
		swagForge.user(userName).keys().exists {
			(_).trim.split("\\s+").toList match
				case _ :: encoded :: _ =>

					// i basically copied GitBucket's approach
					// ... everyone should go marvel at GitBucket ...
					// https://github.com/gitbucket/gitbucket/blob/master/src/main/scala/gitbucket/core/ssh/SshUtil.scala
					import org.apache.sshd.common.util.buffer.ByteArrayBuffer

					import java.util.Base64
					publicKey == new ByteArrayBuffer(
						Base64.getDecoder
							.decode(
								// this is redoing a step i don't 100% understand from jgit
								encoded.toList
									.map {
										(c: Char) =>
											require(
												c <= 127,
												MessageFormat.format("not an ASCII string", encoded))
											c.toByte
									}
									.toArray
							)
					).getRawPublicKey
		}
}
