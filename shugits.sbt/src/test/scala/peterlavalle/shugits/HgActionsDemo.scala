package peterlavalle.shugits

import peterlavalle.*

import java.io.*

object HgActionsDemo:
	def main(args: Array[String]): Unit =
		val hgActions: HgActions =
			HgActions {
				var root: File = File(".").AbsoluteFile
				while (!(root / ".hg").isDirectory)
					root = root.ParentFile
				root
			}

		println("heads and not closed")
		hgActions.log("head() and not closed()")
			.foreach(println)

		println("heads())")
		hgActions.heads()
			.foreach(println)


		println("bookmark")
		hgActions.bookmark()
			.foreach(println)

		println("\n.parent")
		println(hgActions.parent)

		println("\nadd tappy")
		hgActions.bookmark((hgActions.parent.getString("node"))) = "tappy"
		hgActions.bookmark().map("\t" + _)
			.foreach(println)

		println("\nrem tappy")
		hgActions.bookmark -= "tappy"
		hgActions.bookmark().map("\t" + _)
			.foreach(println)

		println("... is that it?")