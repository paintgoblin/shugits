package peterlavalle.shugits

import org.json.JSONObject
import peterlavalle.*

import java.io.File

class ThingTests extends munit.FunSuite {

	test("parse that json") {
		val data =
			new JSONObject()
				.put("a", 9)
				.put("b", "queue")

		case class Kat(a: Int, b: String)

		val read: JSON[Kat] =
			import JSON.*
			for {
				//a <- get[Int]
				//b <- get[String]
				a <- "a"[Int]
				b <- "b"[String]
			} yield {
				Kat(a, b)
			}

		assertEquals(
			read.parse(data),
			Left(Kat(9, "queue"))
		)


		val obtained = 42
		val expected = 42
		assertEquals(obtained, expected)
	}

	val cwd = new File("nope").ParentFile
}
