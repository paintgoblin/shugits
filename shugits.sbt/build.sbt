
val scala3Version: String = "3.4.2"

name := "shugits"

organization := "com.peterlavalle"
version := "2024.06.16-SNAPSHOT"

scalaVersion := scala3Version

resolvers += "Maven Central" at "https://repo1.maven.org/maven2/"


libraryDependencies += "com.github.scopt" %% "scopt" % "4.1.0"
libraryDependencies += "org.apache.sshd" % "sshd-core" % "2.13.2"
libraryDependencies += "net.i2p.crypto" % "eddsa" % "0.3.0"
libraryDependencies += "org.json" % "json" % "20240303"

//logging
libraryDependencies ++= Seq(
    "com.typesafe.scala-logging" %% "scala-logging" % "3.9.5",
    "ch.qos.logback" % "logback-classic" % "1.5.8"
)

//
val jGit: String = "6.10.0.202406032230-r"
libraryDependencies += "org.eclipse.jgit" % "org.eclipse.jgit" % jGit
libraryDependencies += "org.eclipse.jgit" % "org.eclipse.jgit.ssh.jsch" % jGit exclude("com.jcraft", "jsch")
libraryDependencies += "com.github.mwiede" % "jsch" % "0.2.18"


// Custom output path and name for the assembled JAR
assembly / assemblyJarName := "shugits.jar"
assembly / target := {
	val target = file("composed/target/target")
	require(target.isDirectory || target.mkdirs())
	target
}

libraryDependencies += "org.scalameta" %% "munit" % "1.0.0" % Test

// https://stackoverflow.com/questions/54834125/sbt-assembly-deduplicate-module-info-class
(assembly / assemblyMergeStrategy) := {
  case x if x.endsWith("module-info.class") => MergeStrategy.discard
  case x =>
    ((assembly / assemblyMergeStrategy).value)(x)
}
