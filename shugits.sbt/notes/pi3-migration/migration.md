

I needed to migrate between hosts, which should have been as simple as restoring a backup to a new host.
I encountered some issues with the setup but managed to get it working, so I believe these notes may be useful.

I believe the initial obstacles were due to the power supply I was using.
Over the past 10 years, I've "burnt out" at least three SBCs (Single Board Computers).
When the first of my Raspberry Pi 3s died in less than a week, I realized the devices that had failed all failed after prolonged use with the same power adapter.
I replaced the adapter with an official Raspberry Pi adapter and set aside the faulty one (and its latest victim) in a box, awaiting the day an electrician enters my life and shows interest in diagnosing it.

Around September 27th, I had the emotional energy and the equipment to continue.
My initial plan was to back up the Forgejo instance along with the associated "namesake" Mercurial repositories.
This is all covered by the `bakup-hs.sh` script in the repository.
In hindsight, backing up the "token" that ShugGit uses to interact with Forgejo would have been a good idea—it simplifies restoration since the key is tied to the website's database and ... you know the rest.
Outside of the project itself, I also needed my rclone settings for the backup script to work.
Fortunately, copying the rclone configuration entirely to the new host was straightforward.




The new host needed a fully functioning instance of the ShugGit dyad.
With the services down, the `.tar` archive could be extracted over their shared volume, and permissions and fingerprints needed to be setup before the applications could be restarted.
The `ssh://` fingerprints between various hosts needed to be re-established as per setup.
This wasn't particularly challenging, just unexpected and tedious.


The most time-consuming part of the restore process was diagnosing and fixing permissions issues.
At least three parts of the system required specific access and restrictions for various files.
I believe future efforts could be greatly simplified by using the `--same-owner` flag during extraction.
I wasn't aware of this option until writing this up.


For example, the ShugGit container would regularly fail with the error:

```bash
remote: Caused by: com.jcraft.jsch.JSchException: Auth fail for methods 'publickey'
```

At the same time, the Forgejo container was quietly emitting the following warnings:
```bash
Permissions 0670 for '/data/ssh/ssh_host_ed25519_key' are too open.
Permissions 0670 for '/data/ssh/ssh_host_rsa_key' are too open.
Permissions 0670 for '/data/ssh/ssh_host_ecdsa_key' are too open.
```

To fix the ShugGit container, which is less complex, I was able to set most (if not all) permissions to a usable state with the following command on the docker host:

```bash
sudo chown -R 999:systemd-journal target/namesake-repos/
```
It was especially relevant for the `.hg/hgrc` files, which are needed for the hooks.
I did commit and deploy changes (now redundant) that might resolve the hooks issue if the system can write its own files.


In conclusion, the process of host migration with ShugGit involves restoring a backup to a new host and overwriting it.
Various `ssh://` fingerprints need to be re-established, but this mirrors the original setup and doesn't require much explanation.
The most surprising aspect was dealing with file permissions relating to ownership and access.
For those familiar with POSIX systems, this will likely be no surprise.

In short... it's not that hard!
