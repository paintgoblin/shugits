
# Shugits

> This is works-for-me ... not as well as I'd like it to work for others ...

Bug Tracker; https://codeberg.org/paintgoblin/shugits/issues

# Serving Mercurial from Git Servers

Shugits allows you to serve [Mercurial](https://mercurial-scm.org/) repositories from [a Forejo Git server.](https://forgejo.org/)
A full Mercurial repostory is kept on the server, preserving branch names (et al) correctly.
Previously something like this was done by "Kiln" which vanished after becoming only-git and trying to compete with MicroSoft's GitHub.
Shugits fills the gap by enabling seamless Mercurial projects to be exposed to existing Git-based infrastructure.

... and it's WaFeEz than the Mercurial tools I've tried (and failed) to setup on my Pi ...

## How It Works

Shugits operates as a custom (ApacheSSHd) SSH server within a (Docker) container, responding to Mercurial commands.
It mirrors the Mercurial repository to git through the [hg-git extension]() on the server. (... and corrects the head names)
From the user's perspective, SSH connections behave as if they are operating on Mercurial, while HTTP connections remain Git-oriented.
The user interfaces and project administration settings remain unchanged.

## How To

### Install the whole system as intended

- clone the project and run `shugits.sbt/ $ sbt assembly` to get the binary jar and copy it into the right place
	- do this one your workstation
	- someday ... i should add a CI job to automate this ...
	- ... or something like ... `λ java -jar ../sbt-launch.jar assembly`
- copy the `shugits.sbt/composed/` to the server as `shugits-live/`
	- ... actually ... you can use whatever name you want - but - this guide is was easier if i pretend it has to be `shugits-live/`
- make the `shugits-live/target/target/` folder usable to the shugits container
	-  `shugits-live/ $ sudo chgrp systemd-journal target/target/`
	-  `shugits-live/ $ chmod g+=rwx target/target/`
- setup forgejo and your account
	- add any DNS entries
	- `shugits-live/ $ docker-compose up --build forgejo` and leave it running in a separate window for now
	- set the hostname to whatever is sensible
	- use port 5190 for git's ssh in the forgejo config
	- make yourself an admin account
	- setup your (desktop) public ssh key (for testing)
- create an access token key
	* this allows shugits to take control
	- sign in, click your profile, open `Settings` > `Applications` > select all r+w permissions and then `Generate token`
		* weirdly - this is done with your admin account and not something system-wide
		* in theory ... it could work with less permissions, but, i lack the patience to chekc that
	- save the token to a text file `shugits-live/target/target/shugits.token`
		* ... right next to `shugits-live/target/target/shugits.jar` ...
		* i've used WinSCP's text weditor fine for this
- launch the shugits container
	- `shugits-live/ $ docker-compose up --build shugits` and leave it running for now
	* this could take awhile
- create some project on forgejo, then, push to it from hg
	- you'll have to do the normal fingerptin stuff and ssh into the host before THgW will work right (sorry)
	- you'll need to change the ssh from `.../git@...` to  `.../hg@...`
		- ... so ... `ssh://git@sg.peterlavalle.com:5190/peter/shugits.git` becomes `ssh://hg@sg.peterlavalle.com:5190/peter/shugits.git`
- (finally) kill and restart the containers to run on their own
	- CTRL+C both the `forgejo` and `shugits` containers
	- run `shugits-live/ $ docker-compose up -d`
		- shugits will die; sorry
	- navigate to your forgejo page to see if it's up and running yet
	- re-run `shugits-live/ $ docker-compose up -d` and see that `shugits` is up again
- (optional) setup the git project to mirror to somewhere offsite
	- https://codeberg.org/
	- https://gitlab.com/
	- https://sr.ht/
	- maybe? https://bitbucket.org/product
	- i dunno; https://heptapod.net/
	- ... i guess ... https://github.com/
- (ideally) clone peter's git copy and push some fixes or features
	- https://codeberg.org/paintgoblin/shugits
	- at time of writing ... he needs to setup tools to import them
	- ... so ... motivate him with a useful patch!
	- you can *just* run the shugit's `Main` class IntelliJ to get a mostly debuggable copy
		- some stuff happens through mercurial hooks ... i can't help there ... sorry

### cron backups


(wip - should be in a branch but i'm the dumb)





### Reset a password from the command line

- login to the host
- find the container id with `docker ps`
- bash into the container with `docker exec -it 4edeadcafe /bin/bash`
- change to the git user `su git`
- list the forgejo accounts `gitea admin user list`
- change the password ` gitea admin user change-password -u plavalle -p foobar123`
- login and set a real password!

## TODOs

These are lists of things I want to remember to do, grouped by relative priority.


### 0.1 (shared)

- [ ] fix getting git errors out to the hg connection
	- https://codeberg.org/paintgoblin/shugits/issues/2
	- really - this is a bug
	- probably just "do" real logging while i'm in there
- [ ] check for remote-git content, and, if it's there ... insist that the human pull it (via http maybe?) so that no revs are lost
	- https://codeberg.org/paintgoblin/shugits/issues/3
- [ ] warn the user when a head won't be pushed
	- https://codeberg.org/paintgoblin/shugits/issues/4
	- ... this should alreayd happen; diagnose it

### 0.2 (mirror)

- [ ] git passthough
	- https://codeberg.org/paintgoblin/shugits/issues/6
	- ignore the username; react to the command they try to run
- [ ] hg mirror
	- https://codeberg.org/paintgoblin/shugits/issues/7
	- see that existing git-mirror thing? well ...
	- respond to servcer commands at self that try sometihg:port/thing/want.git/hg@somebody:port/paattthhhh
	- push the user/repo's stuff using --config to use a per-user/repo key
	- ... maybe ... maybe this is bad because it will allow fake request?
	- ... so we treat "git mirror of username/repo/all-that-jazz" as a "do a push of user/repo"
	- maybe stash the hg repo as the "username" and the ssh.pub as the "password" THEN respond to the push command by finding the match from the repo, checking it and pushing it?


### 1.0 (collaborative)

- understood
	- [ ] fix names to ensure `_` and `-` and `.` are allowed
		- https://codeberg.org/paintgoblin/shugits/issues/1
	- [ ] lock things (maybe)
		- https://codeberg.org/paintgoblin/shugits/issues/8
		- lock repos during push/pull
		- lock the key during its key thingies
	- [ ] cleanup dead git branches
		- https://codeberg.org/paintgoblin/shugits/issues/9
		- if you check for remote content first, then, this won't risk losing anything
		- find out if the "head" commit for a branch is a closing hg commit; and delete based on that!
- unknown
	- [ ] support groups and collaborators
		- https://codeberg.org/paintgoblin/shugits/issues/10
	- [ ] hunt down all those TODOs
		- https://codeberg.org/paintgoblin/shugits/issues/11

### 1.1 (enduring)

- [ ] clone/mirror/forceup the wiki and tickets so that they can be pushed/pulled as well
	- https://codeberg.org/paintgoblin/shugits/issues/12
	- will lose branch info methinks
- [ ] hg over tags
	- https://codeberg.org/paintgoblin/shugits/issues/13
	- have the hg tags overwrite the git ones (sorry)
	- mildly redundant as hg tags are permanent ... but ...

### Future Work

I dunno ...

- [ ] do the git stuff in some sort of message queue to allow hg connections to finish ASAP
- [ ] read-only ssh connections for hg; copy the project, use the copy for those pesky hg commands, destroy the copy at connection close to ignore any push
	- ... unless hg-serve has something i don't know about
- [ ] some sort of garbage collection or such for the namesake side

https://app.element.io/#/room/#forgejo-chat:matrix.org

## Development

The use of a standard forgejo image negates the need for development of that image.
The image must be running (however) for the shugits server to operate, then the java program can be started from IDEA, then ... ugghh; just recall these future Peter ...

- shell `shugits.sbt/composed/ $ docker-compose up --build forgejo`
- browse; `http://localhost:3000/`
- clear; `shugits.sbt/target/namesake-repos`
- main;  `peterlavalle/shugits/Main.scala`
- push; `hg push -r3 --new-branch ssh://hg@localhost:5190/peter/shugits.git`


### out to hg!

need to get log messages (et al) out to the hg user.
need to keep other messages "less public."

i think that it's a little better than it was before.
not sure what to do with TheHook - maybe it's fine, maybe it should be bash ... maybe it ... IDK
