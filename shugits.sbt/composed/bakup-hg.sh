#!/bin/bash

# Get the directory of the script
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Define the source directory as the script's parent directory (modify as needed)
SOURCE_DIR="$SCRIPT_DIR/target"

# Define the backup directory as the current working directory (PWD)
BACKUP_DIR="$(pwd)"

# Define the backup archive name with a timestamp
BACKUP_NAME="shugits-backups.$(hostname).$(date +%Y%m%d_%H%M%S).tar.gz"

# Change to the source directory to ensure relative paths are used in the tar archive
cd "$SOURCE_DIR"

echo 'shutting down ...'

docker-compose down

echo 'copying now'

# find/copy
#   - the namesake repos
#   - the saved token
#   - the saved token
#   - the .db
tar -czvf "$BACKUP_DIR/$BACKUP_NAME" \
    $(find target/namesake-repos/*/*/hg-namesake -type d -print) \
    target/shugits.token \
    forgejo/gitea/gitea.db \
    forgejo/git/repositories \
    forgejo/git/lfs \
    forgejo/gitea/attachments > /dev/null

echo 'spinning back up ...'

docker-compose up -d

# Print only the backup archive name to stdout
echo "uploading $BACKUP_NAME ..."

# copy it to my gdrive
# ... you'll have to setup rclone for this ...
# ... and when you do; do the auto-web setup step Y then use an second session with an ssh tunnel `-L localhost:53682:localhost:53682`
rclone copy "$BACKUP_DIR/$BACKUP_NAME" grive-shugits:shugits-backups/

# Check if rclone copy was successful
if [ $? -eq 0 ]; then
    # If successful, delete the local backup file
    rm "$BACKUP_DIR/$BACKUP_NAME"
    echo "Backup file $BACKUP_FILE successfully copied and deleted."
else
    echo "Error: rclone copy failed."
fi
